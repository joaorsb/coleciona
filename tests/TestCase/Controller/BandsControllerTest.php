<?php
namespace App\Test\TestCase\Controller;

use App\Controller\BandsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\BandsController Test Case
 */
class BandsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bands',
        'app.countries',
        'app.genres',
        'app.albums',
        'app.users',
        'app.labels',
        'app.artists',
        'app.instruments',
        'app.albums_artists',
        'app.bands_artists',
        'app.songs',
        'app.songs_artists',
        'app.albums_songs',
        'app.songs_bands',
        'app.albums_bands'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
