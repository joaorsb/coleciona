<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MoviesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\MoviesController Test Case
 */
class MoviesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.movies',
        'app.studios',
        'app.actors',
        'app.countries',
        'app.continents',
        'app.actresses',
        'app.movies_actresses',
        'app.artists',
        'app.instruments',
        'app.albums',
        'app.users',
        'app.labels',
        'app.albums_artists',
        'app.bands',
        'app.genres',
        'app.books',
        'app.publishers',
        'app.authors',
        'app.books_authors',
        'app.books_genres',
        'app.tags',
        'app.books_tags',
        'app.albums_bands',
        'app.bands_artists',
        'app.songs',
        'app.songs_bands',
        'app.albums_songs',
        'app.songs_artists',
        'app.directors',
        'app.movies_directors',
        'app.translators',
        'app.movies_actors'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
