<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UsersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\UsersController Test Case
 */
class UsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users',
        'app.albums',
        'app.labels',
        'app.countries',
        'app.continents',
        'app.actors',
        'app.movies',
        'app.studios',
        'app.movies_actors',
        'app.actresses',
        'app.movies_actresses',
        'app.directors',
        'app.movies_directors',
        'app.artists',
        'app.instruments',
        'app.albums_artists',
        'app.bands',
        'app.genres',
        'app.books',
        'app.publishers',
        'app.authors',
        'app.books_authors',
        'app.books_genres',
        'app.tags',
        'app.books_tags',
        'app.albums_bands',
        'app.bands_artists',
        'app.songs',
        'app.albums_songs',
        'app.songs_artists',
        'app.songs_bands',
        'app.translators'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
