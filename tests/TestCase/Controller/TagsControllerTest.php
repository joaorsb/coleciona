<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TagsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\TagsController Test Case
 */
class TagsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tags',
        'app.books',
        'app.users',
        'app.publishers',
        'app.countries',
        'app.continents',
        'app.actors',
        'app.movies',
        'app.studios',
        'app.movies_actors',
        'app.actresses',
        'app.movies_actresses',
        'app.directors',
        'app.movies_directors',
        'app.artists',
        'app.instruments',
        'app.albums',
        'app.labels',
        'app.albums_artists',
        'app.bands',
        'app.genres',
        'app.books_genres',
        'app.albums_bands',
        'app.bands_artists',
        'app.songs',
        'app.albums_songs',
        'app.songs_artists',
        'app.songs_bands',
        'app.authors',
        'app.books_authors',
        'app.translators',
        'app.books_tags'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
