<?php
namespace App\Test\TestCase\Controller;

use App\Controller\InstrumentsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\InstrumentsController Test Case
 */
class InstrumentsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.instruments',
        'app.artists',
        'app.countries',
        'app.continents',
        'app.actors',
        'app.movies',
        'app.movies_actors',
        'app.movies_actresses',
        'app.actresses',
        'app.movies_directors',
        'app.directors',
        'app.authors',
        'app.books',
        'app.users',
        'app.publishers',
        'app.books_authors',
        'app.genres',
        'app.bands',
        'app.albums',
        'app.labels',
        'app.albums_artists',
        'app.albums_bands',
        'app.songs',
        'app.albums_songs',
        'app.songs_bands',
        'app.songs_artists',
        'app.bands_artists',
        'app.books_genres',
        'app.tags',
        'app.books_tags',
        'app.studios',
        'app.translators'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
