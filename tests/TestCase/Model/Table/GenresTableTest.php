<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GenresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GenresTable Test Case
 */
class GenresTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.genres',
        'app.bands',
        'app.countries',
        'app.continents',
        'app.actors',
        'app.movies',
        'app.movies_actors',
        'app.actresses',
        'app.movies_actresses',
        'app.artists',
        'app.instruments',
        'app.albums',
        'app.users',
        'app.labels',
        'app.albums_artists',
        'app.albums_bands',
        'app.songs',
        'app.albums_songs',
        'app.bands_artists',
        'app.songs_artists',
        'app.authors',
        'app.books',
        'app.publishers',
        'app.books_authors',
        'app.books_genres',
        'app.tags',
        'app.books_tags',
        'app.directors',
        'app.movies_directors',
        'app.studios',
        'app.translators',
        'app.songs_bands'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Genres') ? [] : ['className' => 'App\Model\Table\GenresTable'];
        $this->Genres = TableRegistry::get('Genres', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Genres);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
