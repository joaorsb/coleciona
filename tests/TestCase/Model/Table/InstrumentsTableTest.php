<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InstrumentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InstrumentsTable Test Case
 */
class InstrumentsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.instruments',
        'app.artists',
        'app.countries',
        'app.continents',
        'app.actors',
        'app.movies',
        'app.movies_actors',
        'app.actresses',
        'app.movies_actresses',
        'app.authors',
        'app.books',
        'app.users',
        'app.publishers',
        'app.books_authors',
        'app.genres',
        'app.bands',
        'app.albums',
        'app.labels',
        'app.albums_artists',
        'app.albums_bands',
        'app.songs',
        'app.albums_songs',
        'app.bands_artists',
        'app.songs_bands',
        'app.books_genres',
        'app.tags',
        'app.books_tags',
        'app.directors',
        'app.movies_directors',
        'app.studios',
        'app.translators',
        'app.songs_artists'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Instruments') ? [] : ['className' => 'App\Model\Table\InstrumentsTable'];
        $this->Instruments = TableRegistry::get('Instruments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Instruments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
