<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PublishersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PublishersTable Test Case
 */
class PublishersTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.publishers',
        'app.countries',
        'app.continents',
        'app.actors',
        'app.movies',
        'app.studios',
        'app.movies_actors',
        'app.actresses',
        'app.movies_actresses',
        'app.directors',
        'app.movies_directors',
        'app.artists',
        'app.instruments',
        'app.albums',
        'app.users',
        'app.labels',
        'app.albums_artists',
        'app.bands',
        'app.genres',
        'app.books',
        'app.authors',
        'app.books_authors',
        'app.books_genres',
        'app.tags',
        'app.books_tags',
        'app.albums_bands',
        'app.bands_artists',
        'app.songs',
        'app.songs_bands',
        'app.albums_songs',
        'app.songs_artists',
        'app.translators'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Publishers') ? [] : ['className' => 'App\Model\Table\PublishersTable'];
        $this->Publishers = TableRegistry::get('Publishers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Publishers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
