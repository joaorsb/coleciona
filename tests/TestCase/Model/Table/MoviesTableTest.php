<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MoviesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MoviesTable Test Case
 */
class MoviesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.movies',
        'app.studios',
        'app.actors',
        'app.countries',
        'app.continents',
        'app.actresses',
        'app.movies_actresses',
        'app.artists',
        'app.instruments',
        'app.albums',
        'app.users',
        'app.labels',
        'app.albums_artists',
        'app.bands',
        'app.genres',
        'app.books',
        'app.publishers',
        'app.authors',
        'app.books_authors',
        'app.books_genres',
        'app.tags',
        'app.books_tags',
        'app.albums_bands',
        'app.bands_artists',
        'app.songs',
        'app.songs_bands',
        'app.albums_songs',
        'app.songs_artists',
        'app.directors',
        'app.movies_directors',
        'app.translators',
        'app.movies_actors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Movies') ? [] : ['className' => 'App\Model\Table\MoviesTable'];
        $this->Movies = TableRegistry::get('Movies', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Movies);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
