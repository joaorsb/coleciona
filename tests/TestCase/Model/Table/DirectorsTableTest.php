<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DirectorsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DirectorsTable Test Case
 */
class DirectorsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.directors',
        'app.countries',
        'app.continents',
        'app.actors',
        'app.movies',
        'app.movies_actors',
        'app.actresses',
        'app.movies_actresses',
        'app.artists',
        'app.instruments',
        'app.albums',
        'app.users',
        'app.labels',
        'app.albums_artists',
        'app.bands',
        'app.genres',
        'app.albums_bands',
        'app.bands_artists',
        'app.songs',
        'app.songs_bands',
        'app.albums_songs',
        'app.songs_artists',
        'app.authors',
        'app.books',
        'app.publishers',
        'app.books_authors',
        'app.books_genres',
        'app.tags',
        'app.books_tags',
        'app.studios',
        'app.translators',
        'app.movies_directors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Directors') ? [] : ['className' => 'App\Model\Table\DirectorsTable'];
        $this->Directors = TableRegistry::get('Directors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Directors);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
