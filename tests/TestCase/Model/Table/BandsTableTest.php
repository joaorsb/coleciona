<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BandsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BandsTable Test Case
 */
class BandsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bands',
        'app.countries',
        'app.genres',
        'app.albums',
        'app.users',
        'app.labels',
        'app.artists',
        'app.instruments',
        'app.albums_artists',
        'app.bands_artists',
        'app.songs',
        'app.songs_artists',
        'app.albums_bands',
        'app.albums_songs',
        'app.songs_bands'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Bands') ? [] : ['className' => 'App\Model\Table\BandsTable'];
        $this->Bands = TableRegistry::get('Bands', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bands);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
