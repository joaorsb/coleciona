<?php
use Migrations\AbstractMigration;

class CreateSongs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('songs');
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('duration', 'time', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('studio', 'boolean', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('live', 'boolean', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('lyrics', 'text', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
