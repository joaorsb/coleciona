<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Instruments Controller
 *
 * @property \App\Model\Table\InstrumentsTable $Instruments
 */
class InstrumentsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('instruments', $this->paginate($this->Instruments));
        $this->set('_serialize', ['instruments']);
    }

    /**
     * View method
     *
     * @param string|null $id Instrument id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $instrument = $this->Instruments->get($id, [
            'contain' => ['Artists']
        ]);
        $this->set('instrument', $instrument);
        $this->set('_serialize', ['instrument']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $instrument = $this->Instruments->newEntity();
        if ($this->request->is('post')) {
            $instrument = $this->Instruments->patchEntity($instrument, $this->request->data);
            if ($this->Instruments->save($instrument)) {
                $this->Flash->success(__('The instrument has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The instrument could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('instrument'));
        $this->set('_serialize', ['instrument']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Instrument id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $instrument = $this->Instruments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $instrument = $this->Instruments->patchEntity($instrument, $this->request->data);
            if ($this->Instruments->save($instrument)) {
                $this->Flash->success(__('The instrument has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The instrument could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('instrument'));
        $this->set('_serialize', ['instrument']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Instrument id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $instrument = $this->Instruments->get($id);
        if ($this->Instruments->delete($instrument)) {
            $this->Flash->success(__('The instrument has been deleted.'));
        } else {
            $this->Flash->error(__('The instrument could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
