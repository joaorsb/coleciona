<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Movies Controller
 *
 * @property \App\Model\Table\MoviesTable $Movies
 */
class MoviesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Studios']
        ];
        $this->set('movies', $this->paginate($this->Movies));
        $this->set('_serialize', ['movies']);
    }

    /**
     * View method
     *
     * @param string|null $id Movie id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $movie = $this->Movies->get($id, [
            'contain' => ['Studios', 'Actors', 'Actresses', 'Directors']
        ]);
        $this->set('movie', $movie);
        $this->set('_serialize', ['movie']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $movie = $this->Movies->newEntity();
        if ($this->request->is('post')) {
            $movie = $this->Movies->patchEntity($movie, $this->request->data);
            if ($this->Movies->save($movie)) {
                $this->Flash->success(__('The movie has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The movie could not be saved. Please, try again.'));
            }
        }
        $studios = $this->Movies->Studios->find('list', ['limit' => 200]);
        $actors = $this->Movies->Actors->find('list', ['limit' => 200]);
        $actresses = $this->Movies->Actresses->find('list', ['limit' => 200]);
        $directors = $this->Movies->Directors->find('list', ['limit' => 200]);
        $this->set(compact('movie', 'studios', 'actors', 'actresses', 'directors'));
        $this->set('_serialize', ['movie']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Movie id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $movie = $this->Movies->get($id, [
            'contain' => ['Actors', 'Actresses', 'Directors']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $movie = $this->Movies->patchEntity($movie, $this->request->data);
            if ($this->Movies->save($movie)) {
                $this->Flash->success(__('The movie has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The movie could not be saved. Please, try again.'));
            }
        }
        $studios = $this->Movies->Studios->find('list', ['limit' => 200]);
        $actors = $this->Movies->Actors->find('list', ['limit' => 200]);
        $actresses = $this->Movies->Actresses->find('list', ['limit' => 200]);
        $directors = $this->Movies->Directors->find('list', ['limit' => 200]);
        $this->set(compact('movie', 'studios', 'actors', 'actresses', 'directors'));
        $this->set('_serialize', ['movie']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Movie id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $movie = $this->Movies->get($id);
        if ($this->Movies->delete($movie)) {
            $this->Flash->success(__('The movie has been deleted.'));
        } else {
            $this->Flash->error(__('The movie could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
