<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Albums Controller
 *
 * @property \App\Model\Table\AlbumsTable $Albums
 */
class AlbumsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Labels']
        ];
        $this->set('albums', $this->paginate($this->Albums));
        $this->set('_serialize', ['albums']);
    }

    /**
     * View method
     *
     * @param string|null $id Album id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $album = $this->Albums->get($id, [
            'contain' => ['Users', 'Labels', 'Artists', 'Bands', 'Songs']
        ]);
        $this->set('album', $album);
        $this->set('_serialize', ['album']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $album = $this->Albums->newEntity();
        if ($this->request->is('post')) {
            $album = $this->Albums->patchEntity($album, $this->request->data);
            if ($this->Albums->save($album)) {
                $this->Flash->success(__('The album has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The album could not be saved. Please, try again.'));
            }
        }
        $users = $this->Albums->Users->find('list', ['limit' => 200]);
        $labels = $this->Albums->Labels->find('list', ['limit' => 200]);
        $artists = $this->Albums->Artists->find('list', ['limit' => 200]);
        $bands = $this->Albums->Bands->find('list', ['limit' => 200]);
        $songs = $this->Albums->Songs->find('list', ['limit' => 200]);
        $this->set(compact('album', 'users', 'labels', 'artists', 'bands', 'songs'));
        $this->set('_serialize', ['album']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Album id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $album = $this->Albums->get($id, [
            'contain' => ['Artists', 'Bands', 'Songs']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $album = $this->Albums->patchEntity($album, $this->request->data);
            if ($this->Albums->save($album)) {
                $this->Flash->success(__('The album has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The album could not be saved. Please, try again.'));
            }
        }
        $users = $this->Albums->Users->find('list', ['limit' => 200]);
        $labels = $this->Albums->Labels->find('list', ['limit' => 200]);
        $artists = $this->Albums->Artists->find('list', ['limit' => 200]);
        $bands = $this->Albums->Bands->find('list', ['limit' => 200]);
        $songs = $this->Albums->Songs->find('list', ['limit' => 200]);
        $this->set(compact('album', 'users', 'labels', 'artists', 'bands', 'songs'));
        $this->set('_serialize', ['album']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Album id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $album = $this->Albums->get($id);
        if ($this->Albums->delete($album)) {
            $this->Flash->success(__('The album has been deleted.'));
        } else {
            $this->Flash->error(__('The album could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
