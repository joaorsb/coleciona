<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Actresses Controller
 *
 * @property \App\Model\Table\ActressesTable $Actresses
 */
class ActressesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Countries']
        ];
        $this->set('actresses', $this->paginate($this->Actresses));
        $this->set('_serialize', ['actresses']);
    }

    /**
     * View method
     *
     * @param string|null $id Actress id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $actress = $this->Actresses->get($id, [
            'contain' => ['Countries', 'Movies']
        ]);
        $this->set('actress', $actress);
        $this->set('_serialize', ['actress']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $actress = $this->Actresses->newEntity();
        if ($this->request->is('post')) {
            $actress = $this->Actresses->patchEntity($actress, $this->request->data);
            if ($this->Actresses->save($actress)) {
                $this->Flash->success(__('The actress has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The actress could not be saved. Please, try again.'));
            }
        }
        $countries = $this->Actresses->Countries->find('list', ['limit' => 200]);
        $movies = $this->Actresses->Movies->find('list', ['limit' => 200]);
        $this->set(compact('actress', 'countries', 'movies'));
        $this->set('_serialize', ['actress']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Actress id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $actress = $this->Actresses->get($id, [
            'contain' => ['Movies']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $actress = $this->Actresses->patchEntity($actress, $this->request->data);
            if ($this->Actresses->save($actress)) {
                $this->Flash->success(__('The actress has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The actress could not be saved. Please, try again.'));
            }
        }
        $countries = $this->Actresses->Countries->find('list', ['limit' => 200]);
        $movies = $this->Actresses->Movies->find('list', ['limit' => 200]);
        $this->set(compact('actress', 'countries', 'movies'));
        $this->set('_serialize', ['actress']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Actress id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $actress = $this->Actresses->get($id);
        if ($this->Actresses->delete($actress)) {
            $this->Flash->success(__('The actress has been deleted.'));
        } else {
            $this->Flash->error(__('The actress could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
