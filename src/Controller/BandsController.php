<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Bands Controller
 *
 * @property \App\Model\Table\BandsTable $Bands
 */
class BandsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Countries', 'Genres']
        ];
        $this->set('bands', $this->paginate($this->Bands));
        $this->set('_serialize', ['bands']);
    }

    /**
     * View method
     *
     * @param string|null $id Band id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $band = $this->Bands->get($id, [
            'contain' => ['Countries', 'Genres', 'Albums', 'Artists', 'Songs']
        ]);
        $this->set('band', $band);
        $this->set('_serialize', ['band']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $band = $this->Bands->newEntity();
        if ($this->request->is('post')) {
            $band = $this->Bands->patchEntity($band, $this->request->data);
            if ($this->Bands->save($band)) {
                $this->Flash->success(__('The band has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The band could not be saved. Please, try again.'));
            }
        }
        $countries = $this->Bands->Countries->find('list', ['limit' => 200]);
        $genres = $this->Bands->Genres->find('list', ['limit' => 200]);
        $albums = $this->Bands->Albums->find('list', ['limit' => 200]);
        $artists = $this->Bands->Artists->find('list', ['limit' => 200]);
        $songs = $this->Bands->Songs->find('list', ['limit' => 200]);
        $this->set(compact('band', 'countries', 'genres', 'albums', 'artists', 'songs'));
        $this->set('_serialize', ['band']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Band id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $band = $this->Bands->get($id, [
            'contain' => ['Albums', 'Artists', 'Songs']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $band = $this->Bands->patchEntity($band, $this->request->data);
            if ($this->Bands->save($band)) {
                $this->Flash->success(__('The band has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The band could not be saved. Please, try again.'));
            }
        }
        $countries = $this->Bands->Countries->find('list', ['limit' => 200]);
        $genres = $this->Bands->Genres->find('list', ['limit' => 200]);
        $albums = $this->Bands->Albums->find('list', ['limit' => 200]);
        $artists = $this->Bands->Artists->find('list', ['limit' => 200]);
        $songs = $this->Bands->Songs->find('list', ['limit' => 200]);
        $this->set(compact('band', 'countries', 'genres', 'albums', 'artists', 'songs'));
        $this->set('_serialize', ['band']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Band id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $band = $this->Bands->get($id);
        if ($this->Bands->delete($band)) {
            $this->Flash->success(__('The band has been deleted.'));
        } else {
            $this->Flash->error(__('The band could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
