<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Albums'), ['controller' => 'Albums', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('List Books'), ['controller' => 'Books', 'action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name) ?></h3>
    <?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id], ['class' => 'right']) ?>
    <?= $this->Form->button(__('Delete User'), ['action' => 'delete', $user->id], ['class' => 'right', 'confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
    <table class="table">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Email') ?></th>
            <th><?= __('Created') ?></th>
            <th><?= __('Modified') ?></th>
        </tr>
        <tr>
            <td><?= $this->Number->format($user->id) ?></td>
            <td><?= h($user->email) ?></td>
            <td><?= h($user->created) ?></td>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Albums') ?></h4>
        <?php if (!empty($user->albums)): ?>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Name') ?></th>
                    <th><?= __('User Id') ?></th>
                    <th><?= __('Label Id') ?></th>
                    <th><?= __('Release Year') ?></th>
                    <th><?= __('Studio') ?></th>
                    <th><?= __('Live') ?></th>
                    <th><?= __('Created') ?></th>
                    <th><?= __('Modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($user->albums as $albums): ?>
                    <tr>
                        <td><?= h($albums->id) ?></td>
                        <td><?= h($albums->name) ?></td>
                        <td><?= h($albums->user_id) ?></td>
                        <td><?= h($albums->label_id) ?></td>
                        <td><?= h($albums->release_year) ?></td>
                        <td><?= h($albums->studio) ?></td>
                        <td><?= h($albums->live) ?></td>
                        <td><?= h($albums->created) ?></td>
                        <td><?= h($albums->modified) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['controller' => 'Albums', 'action' => 'view', $albums->id]) ?>

                            <?= $this->Html->link(__('Edit'), ['controller' => 'Albums', 'action' => 'edit', $albums->id]) ?>

                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'Albums', 'action' => 'delete', $albums->id], ['confirm' => __('Are you sure you want to delete # {0}?', $albums->id)]) ?>

                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Books') ?></h4>
        <?php if (!empty($user->books)): ?>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Name') ?></th>
                    <th><?= __('User Id') ?></th>
                    <th><?= __('Publisher Id') ?></th>
                    <th><?= __('Number Of Pages') ?></th>
                    <th><?= __('Sinopsis') ?></th>
                    <th><?= __('Isbn') ?></th>
                    <th><?= __('Created') ?></th>
                    <th><?= __('Modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($user->books as $books): ?>
                    <tr>
                        <td><?= h($books->id) ?></td>
                        <td><?= h($books->name) ?></td>
                        <td><?= h($books->user_id) ?></td>
                        <td><?= h($books->publisher_id) ?></td>
                        <td><?= h($books->number_of_pages) ?></td>
                        <td><?= h($books->sinopsis) ?></td>
                        <td><?= h($books->isbn) ?></td>
                        <td><?= h($books->created) ?></td>
                        <td><?= h($books->modified) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['controller' => 'Books', 'action' => 'view', $books->id]) ?>

                            <?= $this->Html->link(__('Edit'), ['controller' => 'Books', 'action' => 'edit', $books->id]) ?>

                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'Books', 'action' => 'delete', $books->id], ['confirm' => __('Are you sure you want to delete # {0}?', $books->id)]) ?>

                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>
