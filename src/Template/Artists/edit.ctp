<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $artist->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $artist->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Artists'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Instruments'), ['controller' => 'Instruments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Instrument'), ['controller' => 'Instruments', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Albums'), ['controller' => 'Albums', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Album'), ['controller' => 'Albums', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Bands'), ['controller' => 'Bands', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Band'), ['controller' => 'Bands', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Songs'), ['controller' => 'Songs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Song'), ['controller' => 'Songs', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="artists form large-9 medium-8 columns content">
    <?= $this->Form->create($artist) ?>
    <fieldset>
        <legend><?= __('Edit Artist') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('country_id', ['options' => $countries]);
            echo $this->Form->input('birth');
            echo $this->Form->input('death');
            echo $this->Form->input('description');
            echo $this->Form->input('instrument_id', ['options' => $instruments]);
            echo $this->Form->input('facebook');
            echo $this->Form->input('twitter');
            echo $this->Form->input('site');
            echo $this->Form->input('albums._ids', ['options' => $albums]);
            echo $this->Form->input('bands._ids', ['options' => $bands]);
            echo $this->Form->input('songs._ids', ['options' => $songs]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
