<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Artist'), ['action' => 'edit', $artist->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Artist'), ['action' => 'delete', $artist->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artist->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Artists'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artist'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Instruments'), ['controller' => 'Instruments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Instrument'), ['controller' => 'Instruments', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Albums'), ['controller' => 'Albums', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Album'), ['controller' => 'Albums', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Bands'), ['controller' => 'Bands', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Band'), ['controller' => 'Bands', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Songs'), ['controller' => 'Songs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Song'), ['controller' => 'Songs', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="artists view large-9 medium-8 columns content">
    <h3><?= h($artist->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($artist->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Country') ?></th>
            <td><?= $artist->has('country') ? $this->Html->link($artist->country->name, ['controller' => 'Countries', 'action' => 'view', $artist->country->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Instrument') ?></th>
            <td><?= $artist->has('instrument') ? $this->Html->link($artist->instrument->name, ['controller' => 'Instruments', 'action' => 'view', $artist->instrument->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Facebook') ?></th>
            <td><?= h($artist->facebook) ?></td>
        </tr>
        <tr>
            <th><?= __('Twitter') ?></th>
            <td><?= h($artist->twitter) ?></td>
        </tr>
        <tr>
            <th><?= __('Site') ?></th>
            <td><?= h($artist->site) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($artist->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Birth') ?></th>
            <td><?= h($artist->birth) ?></td>
        </tr>
        <tr>
            <th><?= __('Death') ?></th>
            <td><?= h($artist->death) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($artist->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($artist->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($artist->description)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Albums') ?></h4>
        <?php if (!empty($artist->albums)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Label Id') ?></th>
                <th><?= __('Release Year') ?></th>
                <th><?= __('Studio') ?></th>
                <th><?= __('Live') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artist->albums as $albums): ?>
            <tr>
                <td><?= h($albums->id) ?></td>
                <td><?= h($albums->name) ?></td>
                <td><?= h($albums->user_id) ?></td>
                <td><?= h($albums->label_id) ?></td>
                <td><?= h($albums->release_year) ?></td>
                <td><?= h($albums->studio) ?></td>
                <td><?= h($albums->live) ?></td>
                <td><?= h($albums->created) ?></td>
                <td><?= h($albums->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Albums', 'action' => 'view', $albums->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Albums', 'action' => 'edit', $albums->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Albums', 'action' => 'delete', $albums->id], ['confirm' => __('Are you sure you want to delete # {0}?', $albums->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Bands') ?></h4>
        <?php if (!empty($artist->bands)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Started At') ?></th>
                <th><?= __('Ended At') ?></th>
                <th><?= __('Genre Id') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Site') ?></th>
                <th><?= __('Facebook') ?></th>
                <th><?= __('Twitter') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artist->bands as $bands): ?>
            <tr>
                <td><?= h($bands->id) ?></td>
                <td><?= h($bands->name) ?></td>
                <td><?= h($bands->country_id) ?></td>
                <td><?= h($bands->started_at) ?></td>
                <td><?= h($bands->ended_at) ?></td>
                <td><?= h($bands->genre_id) ?></td>
                <td><?= h($bands->description) ?></td>
                <td><?= h($bands->site) ?></td>
                <td><?= h($bands->facebook) ?></td>
                <td><?= h($bands->twitter) ?></td>
                <td><?= h($bands->created) ?></td>
                <td><?= h($bands->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Bands', 'action' => 'view', $bands->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Bands', 'action' => 'edit', $bands->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Bands', 'action' => 'delete', $bands->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bands->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Songs') ?></h4>
        <?php if (!empty($artist->songs)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Duration') ?></th>
                <th><?= __('Studio') ?></th>
                <th><?= __('Live') ?></th>
                <th><?= __('Lyrics') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($artist->songs as $songs): ?>
            <tr>
                <td><?= h($songs->id) ?></td>
                <td><?= h($songs->name) ?></td>
                <td><?= h($songs->duration) ?></td>
                <td><?= h($songs->studio) ?></td>
                <td><?= h($songs->live) ?></td>
                <td><?= h($songs->lyrics) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Songs', 'action' => 'view', $songs->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Songs', 'action' => 'edit', $songs->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Songs', 'action' => 'delete', $songs->id], ['confirm' => __('Are you sure you want to delete # {0}?', $songs->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
