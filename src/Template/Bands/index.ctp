<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Band'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Genres'), ['controller' => 'Genres', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Genre'), ['controller' => 'Genres', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Albums'), ['controller' => 'Albums', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Album'), ['controller' => 'Albums', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Artists'), ['controller' => 'Artists', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Artist'), ['controller' => 'Artists', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Songs'), ['controller' => 'Songs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Song'), ['controller' => 'Songs', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bands index large-9 medium-8 columns content">
    <h3><?= __('Bands') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('country_id') ?></th>
                <th><?= $this->Paginator->sort('started_at') ?></th>
                <th><?= $this->Paginator->sort('ended_at') ?></th>
                <th><?= $this->Paginator->sort('genre_id') ?></th>
                <th><?= $this->Paginator->sort('site') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($bands as $band): ?>
            <tr>
                <td><?= $this->Number->format($band->id) ?></td>
                <td><?= h($band->name) ?></td>
                <td><?= $band->has('country') ? $this->Html->link($band->country->name, ['controller' => 'Countries', 'action' => 'view', $band->country->id]) : '' ?></td>
                <td><?= h($band->started_at) ?></td>
                <td><?= h($band->ended_at) ?></td>
                <td><?= $band->has('genre') ? $this->Html->link($band->genre->name, ['controller' => 'Genres', 'action' => 'view', $band->genre->id]) : '' ?></td>
                <td><?= h($band->site) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $band->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $band->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $band->id], ['confirm' => __('Are you sure you want to delete # {0}?', $band->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
