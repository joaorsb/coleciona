<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Movie'), ['action' => 'edit', $movie->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Movie'), ['action' => 'delete', $movie->id], ['confirm' => __('Are you sure you want to delete # {0}?', $movie->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Movies'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Movie'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Studios'), ['controller' => 'Studios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Studio'), ['controller' => 'Studios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Actors'), ['controller' => 'Actors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Actor'), ['controller' => 'Actors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Actresses'), ['controller' => 'Actresses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Actress'), ['controller' => 'Actresses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Directors'), ['controller' => 'Directors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Director'), ['controller' => 'Directors', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="movies view large-9 medium-8 columns content">
    <h3><?= h($movie->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Title') ?></th>
            <td><?= h($movie->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Original Title') ?></th>
            <td><?= h($movie->original_title) ?></td>
        </tr>
        <tr>
            <th><?= __('Studio') ?></th>
            <td><?= $movie->has('studio') ? $this->Html->link($movie->studio->name, ['controller' => 'Studios', 'action' => 'view', $movie->studio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($movie->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Movie Region') ?></th>
            <td><?= $this->Number->format($movie->movie_region) ?></td>
        </tr>
        <tr>
            <th><?= __('Release Year') ?></th>
            <td><?= h($movie->release_year) ?></td>
        </tr>
        <tr>
            <th><?= __('Duration') ?></th>
            <td><?= h($movie->duration) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($movie->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($movie->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Sinopsis') ?></h4>
        <?= $this->Text->autoParagraph(h($movie->sinopsis)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Actors') ?></h4>
        <?php if (!empty($movie->actors)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Birth') ?></th>
                <th><?= __('Death') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($movie->actors as $actors): ?>
            <tr>
                <td><?= h($actors->id) ?></td>
                <td><?= h($actors->name) ?></td>
                <td><?= h($actors->country_id) ?></td>
                <td><?= h($actors->birth) ?></td>
                <td><?= h($actors->death) ?></td>
                <td><?= h($actors->description) ?></td>
                <td><?= h($actors->created) ?></td>
                <td><?= h($actors->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Actors', 'action' => 'view', $actors->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Actors', 'action' => 'edit', $actors->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Actors', 'action' => 'delete', $actors->id], ['confirm' => __('Are you sure you want to delete # {0}?', $actors->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Actresses') ?></h4>
        <?php if (!empty($movie->actresses)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Birth') ?></th>
                <th><?= __('Death') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($movie->actresses as $actresses): ?>
            <tr>
                <td><?= h($actresses->id) ?></td>
                <td><?= h($actresses->name) ?></td>
                <td><?= h($actresses->country_id) ?></td>
                <td><?= h($actresses->birth) ?></td>
                <td><?= h($actresses->death) ?></td>
                <td><?= h($actresses->description) ?></td>
                <td><?= h($actresses->created) ?></td>
                <td><?= h($actresses->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Actresses', 'action' => 'view', $actresses->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Actresses', 'action' => 'edit', $actresses->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Actresses', 'action' => 'delete', $actresses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $actresses->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Directors') ?></h4>
        <?php if (!empty($movie->directors)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Birth') ?></th>
                <th><?= __('Death') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($movie->directors as $directors): ?>
            <tr>
                <td><?= h($directors->id) ?></td>
                <td><?= h($directors->name) ?></td>
                <td><?= h($directors->country_id) ?></td>
                <td><?= h($directors->birth) ?></td>
                <td><?= h($directors->death) ?></td>
                <td><?= h($directors->description) ?></td>
                <td><?= h($directors->created) ?></td>
                <td><?= h($directors->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Directors', 'action' => 'view', $directors->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Directors', 'action' => 'edit', $directors->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Directors', 'action' => 'delete', $directors->id], ['confirm' => __('Are you sure you want to delete # {0}?', $directors->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
