<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Song'), ['action' => 'edit', $song->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Song'), ['action' => 'delete', $song->id], ['confirm' => __('Are you sure you want to delete # {0}?', $song->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Songs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Song'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Albums'), ['controller' => 'Albums', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Album'), ['controller' => 'Albums', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Artists'), ['controller' => 'Artists', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artist'), ['controller' => 'Artists', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Bands'), ['controller' => 'Bands', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Band'), ['controller' => 'Bands', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="songs view large-9 medium-8 columns content">
    <h3><?= h($song->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($song->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($song->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Duration') ?></th>
            <td><?= h($song->duration) ?></td>
        </tr>
        <tr>
            <th><?= __('Studio') ?></th>
            <td><?= $song->studio ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Live') ?></th>
            <td><?= $song->live ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
    <div class="row">
        <h4><?= __('Lyrics') ?></h4>
        <?= $this->Text->autoParagraph(h($song->lyrics)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Albums') ?></h4>
        <?php if (!empty($song->albums)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Label Id') ?></th>
                <th><?= __('Release Year') ?></th>
                <th><?= __('Studio') ?></th>
                <th><?= __('Live') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($song->albums as $albums): ?>
            <tr>
                <td><?= h($albums->id) ?></td>
                <td><?= h($albums->name) ?></td>
                <td><?= h($albums->user_id) ?></td>
                <td><?= h($albums->label_id) ?></td>
                <td><?= h($albums->release_year) ?></td>
                <td><?= h($albums->studio) ?></td>
                <td><?= h($albums->live) ?></td>
                <td><?= h($albums->created) ?></td>
                <td><?= h($albums->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Albums', 'action' => 'view', $albums->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Albums', 'action' => 'edit', $albums->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Albums', 'action' => 'delete', $albums->id], ['confirm' => __('Are you sure you want to delete # {0}?', $albums->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Artists') ?></h4>
        <?php if (!empty($song->artists)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Birth') ?></th>
                <th><?= __('Death') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Instrument Id') ?></th>
                <th><?= __('Facebook') ?></th>
                <th><?= __('Twitter') ?></th>
                <th><?= __('Site') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($song->artists as $artists): ?>
            <tr>
                <td><?= h($artists->id) ?></td>
                <td><?= h($artists->name) ?></td>
                <td><?= h($artists->country_id) ?></td>
                <td><?= h($artists->birth) ?></td>
                <td><?= h($artists->death) ?></td>
                <td><?= h($artists->description) ?></td>
                <td><?= h($artists->instrument_id) ?></td>
                <td><?= h($artists->facebook) ?></td>
                <td><?= h($artists->twitter) ?></td>
                <td><?= h($artists->site) ?></td>
                <td><?= h($artists->created) ?></td>
                <td><?= h($artists->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Artists', 'action' => 'view', $artists->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Artists', 'action' => 'edit', $artists->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Artists', 'action' => 'delete', $artists->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artists->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Bands') ?></h4>
        <?php if (!empty($song->bands)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Started At') ?></th>
                <th><?= __('Ended At') ?></th>
                <th><?= __('Genre Id') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Site') ?></th>
                <th><?= __('Facebook') ?></th>
                <th><?= __('Twitter') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($song->bands as $bands): ?>
            <tr>
                <td><?= h($bands->id) ?></td>
                <td><?= h($bands->name) ?></td>
                <td><?= h($bands->country_id) ?></td>
                <td><?= h($bands->started_at) ?></td>
                <td><?= h($bands->ended_at) ?></td>
                <td><?= h($bands->genre_id) ?></td>
                <td><?= h($bands->description) ?></td>
                <td><?= h($bands->site) ?></td>
                <td><?= h($bands->facebook) ?></td>
                <td><?= h($bands->twitter) ?></td>
                <td><?= h($bands->created) ?></td>
                <td><?= h($bands->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Bands', 'action' => 'view', $bands->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Bands', 'action' => 'edit', $bands->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Bands', 'action' => 'delete', $bands->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bands->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
