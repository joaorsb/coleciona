<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Translator'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="translators index large-9 medium-8 columns content">
    <h3><?= __('Translators') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('country_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($translators as $translator): ?>
            <tr>
                <td><?= $this->Number->format($translator->id) ?></td>
                <td><?= h($translator->name) ?></td>
                <td><?= $translator->has('country') ? $this->Html->link($translator->country->name, ['controller' => 'Countries', 'action' => 'view', $translator->country->id]) : '' ?></td>
                <td><?= h($translator->created) ?></td>
                <td><?= h($translator->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $translator->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $translator->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $translator->id], ['confirm' => __('Are you sure you want to delete # {0}?', $translator->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
