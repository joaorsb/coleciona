<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Country'), ['action' => 'edit', $country->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Country'), ['action' => 'delete', $country->id], ['confirm' => __('Are you sure you want to delete # {0}?', $country->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Countries'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Country'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Continents'), ['controller' => 'Continents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Continent'), ['controller' => 'Continents', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Actors'), ['controller' => 'Actors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Actor'), ['controller' => 'Actors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Actresses'), ['controller' => 'Actresses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Actress'), ['controller' => 'Actresses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Artists'), ['controller' => 'Artists', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artist'), ['controller' => 'Artists', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Bands'), ['controller' => 'Bands', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Band'), ['controller' => 'Bands', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Directors'), ['controller' => 'Directors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Director'), ['controller' => 'Directors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Labels'), ['controller' => 'Labels', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Label'), ['controller' => 'Labels', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Publishers'), ['controller' => 'Publishers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Publisher'), ['controller' => 'Publishers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Studios'), ['controller' => 'Studios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Studio'), ['controller' => 'Studios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Translators'), ['controller' => 'Translators', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Translator'), ['controller' => 'Translators', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="countries view large-9 medium-8 columns content">
    <h3><?= h($country->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($country->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Continent') ?></th>
            <td><?= $country->has('continent') ? $this->Html->link($country->continent->name, ['controller' => 'Continents', 'action' => 'view', $country->continent->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($country->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($country->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($country->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($country->description)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Actors') ?></h4>
        <?php if (!empty($country->actors)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Birth') ?></th>
                <th><?= __('Death') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($country->actors as $actors): ?>
            <tr>
                <td><?= h($actors->id) ?></td>
                <td><?= h($actors->name) ?></td>
                <td><?= h($actors->country_id) ?></td>
                <td><?= h($actors->birth) ?></td>
                <td><?= h($actors->death) ?></td>
                <td><?= h($actors->description) ?></td>
                <td><?= h($actors->created) ?></td>
                <td><?= h($actors->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Actors', 'action' => 'view', $actors->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Actors', 'action' => 'edit', $actors->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Actors', 'action' => 'delete', $actors->id], ['confirm' => __('Are you sure you want to delete # {0}?', $actors->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Actresses') ?></h4>
        <?php if (!empty($country->actresses)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Birth') ?></th>
                <th><?= __('Death') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($country->actresses as $actresses): ?>
            <tr>
                <td><?= h($actresses->id) ?></td>
                <td><?= h($actresses->name) ?></td>
                <td><?= h($actresses->country_id) ?></td>
                <td><?= h($actresses->birth) ?></td>
                <td><?= h($actresses->death) ?></td>
                <td><?= h($actresses->description) ?></td>
                <td><?= h($actresses->created) ?></td>
                <td><?= h($actresses->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Actresses', 'action' => 'view', $actresses->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Actresses', 'action' => 'edit', $actresses->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Actresses', 'action' => 'delete', $actresses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $actresses->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Artists') ?></h4>
        <?php if (!empty($country->artists)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Birth') ?></th>
                <th><?= __('Death') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Instrument Id') ?></th>
                <th><?= __('Facebook') ?></th>
                <th><?= __('Twitter') ?></th>
                <th><?= __('Site') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($country->artists as $artists): ?>
            <tr>
                <td><?= h($artists->id) ?></td>
                <td><?= h($artists->name) ?></td>
                <td><?= h($artists->country_id) ?></td>
                <td><?= h($artists->birth) ?></td>
                <td><?= h($artists->death) ?></td>
                <td><?= h($artists->description) ?></td>
                <td><?= h($artists->instrument_id) ?></td>
                <td><?= h($artists->facebook) ?></td>
                <td><?= h($artists->twitter) ?></td>
                <td><?= h($artists->site) ?></td>
                <td><?= h($artists->created) ?></td>
                <td><?= h($artists->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Artists', 'action' => 'view', $artists->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Artists', 'action' => 'edit', $artists->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Artists', 'action' => 'delete', $artists->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artists->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Authors') ?></h4>
        <?php if (!empty($country->authors)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Birth') ?></th>
                <th><?= __('Death') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($country->authors as $authors): ?>
            <tr>
                <td><?= h($authors->id) ?></td>
                <td><?= h($authors->name) ?></td>
                <td><?= h($authors->country_id) ?></td>
                <td><?= h($authors->description) ?></td>
                <td><?= h($authors->birth) ?></td>
                <td><?= h($authors->death) ?></td>
                <td><?= h($authors->created) ?></td>
                <td><?= h($authors->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Authors', 'action' => 'view', $authors->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Authors', 'action' => 'edit', $authors->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Authors', 'action' => 'delete', $authors->id], ['confirm' => __('Are you sure you want to delete # {0}?', $authors->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Bands') ?></h4>
        <?php if (!empty($country->bands)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Started At') ?></th>
                <th><?= __('Ended At') ?></th>
                <th><?= __('Genre Id') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Site') ?></th>
                <th><?= __('Facebook') ?></th>
                <th><?= __('Twitter') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($country->bands as $bands): ?>
            <tr>
                <td><?= h($bands->id) ?></td>
                <td><?= h($bands->name) ?></td>
                <td><?= h($bands->country_id) ?></td>
                <td><?= h($bands->started_at) ?></td>
                <td><?= h($bands->ended_at) ?></td>
                <td><?= h($bands->genre_id) ?></td>
                <td><?= h($bands->description) ?></td>
                <td><?= h($bands->site) ?></td>
                <td><?= h($bands->facebook) ?></td>
                <td><?= h($bands->twitter) ?></td>
                <td><?= h($bands->created) ?></td>
                <td><?= h($bands->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Bands', 'action' => 'view', $bands->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Bands', 'action' => 'edit', $bands->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Bands', 'action' => 'delete', $bands->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bands->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Directors') ?></h4>
        <?php if (!empty($country->directors)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Birth') ?></th>
                <th><?= __('Death') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($country->directors as $directors): ?>
            <tr>
                <td><?= h($directors->id) ?></td>
                <td><?= h($directors->name) ?></td>
                <td><?= h($directors->country_id) ?></td>
                <td><?= h($directors->birth) ?></td>
                <td><?= h($directors->death) ?></td>
                <td><?= h($directors->description) ?></td>
                <td><?= h($directors->created) ?></td>
                <td><?= h($directors->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Directors', 'action' => 'view', $directors->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Directors', 'action' => 'edit', $directors->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Directors', 'action' => 'delete', $directors->id], ['confirm' => __('Are you sure you want to delete # {0}?', $directors->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Labels') ?></h4>
        <?php if (!empty($country->labels)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($country->labels as $labels): ?>
            <tr>
                <td><?= h($labels->id) ?></td>
                <td><?= h($labels->name) ?></td>
                <td><?= h($labels->description) ?></td>
                <td><?= h($labels->country_id) ?></td>
                <td><?= h($labels->created) ?></td>
                <td><?= h($labels->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Labels', 'action' => 'view', $labels->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Labels', 'action' => 'edit', $labels->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Labels', 'action' => 'delete', $labels->id], ['confirm' => __('Are you sure you want to delete # {0}?', $labels->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Publishers') ?></h4>
        <?php if (!empty($country->publishers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Website') ?></th>
                <th><?= __('Twitter') ?></th>
                <th><?= __('Facebook') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($country->publishers as $publishers): ?>
            <tr>
                <td><?= h($publishers->id) ?></td>
                <td><?= h($publishers->name) ?></td>
                <td><?= h($publishers->country_id) ?></td>
                <td><?= h($publishers->description) ?></td>
                <td><?= h($publishers->email) ?></td>
                <td><?= h($publishers->website) ?></td>
                <td><?= h($publishers->twitter) ?></td>
                <td><?= h($publishers->facebook) ?></td>
                <td><?= h($publishers->created) ?></td>
                <td><?= h($publishers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Publishers', 'action' => 'view', $publishers->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Publishers', 'action' => 'edit', $publishers->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Publishers', 'action' => 'delete', $publishers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $publishers->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Studios') ?></h4>
        <?php if (!empty($country->studios)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Opening') ?></th>
                <th><?= __('Closing') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($country->studios as $studios): ?>
            <tr>
                <td><?= h($studios->id) ?></td>
                <td><?= h($studios->name) ?></td>
                <td><?= h($studios->country_id) ?></td>
                <td><?= h($studios->opening) ?></td>
                <td><?= h($studios->closing) ?></td>
                <td><?= h($studios->description) ?></td>
                <td><?= h($studios->created) ?></td>
                <td><?= h($studios->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Studios', 'action' => 'view', $studios->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Studios', 'action' => 'edit', $studios->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Studios', 'action' => 'delete', $studios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $studios->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Translators') ?></h4>
        <?php if (!empty($country->translators)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($country->translators as $translators): ?>
            <tr>
                <td><?= h($translators->id) ?></td>
                <td><?= h($translators->name) ?></td>
                <td><?= h($translators->country_id) ?></td>
                <td><?= h($translators->description) ?></td>
                <td><?= h($translators->created) ?></td>
                <td><?= h($translators->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Translators', 'action' => 'view', $translators->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Translators', 'action' => 'edit', $translators->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Translators', 'action' => 'delete', $translators->id], ['confirm' => __('Are you sure you want to delete # {0}?', $translators->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
