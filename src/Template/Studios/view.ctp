<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Studio'), ['action' => 'edit', $studio->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Studio'), ['action' => 'delete', $studio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $studio->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Studios'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Studio'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Movies'), ['controller' => 'Movies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Movie'), ['controller' => 'Movies', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="studios view large-9 medium-8 columns content">
    <h3><?= h($studio->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($studio->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Country') ?></th>
            <td><?= $studio->has('country') ? $this->Html->link($studio->country->name, ['controller' => 'Countries', 'action' => 'view', $studio->country->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($studio->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Opening') ?></th>
            <td><?= h($studio->opening) ?></td>
        </tr>
        <tr>
            <th><?= __('Closing') ?></th>
            <td><?= h($studio->closing) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($studio->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($studio->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($studio->description)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Movies') ?></h4>
        <?php if (!empty($studio->movies)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Original Title') ?></th>
                <th><?= __('Release Year') ?></th>
                <th><?= __('Sinopsis') ?></th>
                <th><?= __('Studio Id') ?></th>
                <th><?= __('Movie Region') ?></th>
                <th><?= __('Duration') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($studio->movies as $movies): ?>
            <tr>
                <td><?= h($movies->id) ?></td>
                <td><?= h($movies->title) ?></td>
                <td><?= h($movies->original_title) ?></td>
                <td><?= h($movies->release_year) ?></td>
                <td><?= h($movies->sinopsis) ?></td>
                <td><?= h($movies->studio_id) ?></td>
                <td><?= h($movies->movie_region) ?></td>
                <td><?= h($movies->duration) ?></td>
                <td><?= h($movies->created) ?></td>
                <td><?= h($movies->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Movies', 'action' => 'view', $movies->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Movies', 'action' => 'edit', $movies->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Movies', 'action' => 'delete', $movies->id], ['confirm' => __('Are you sure you want to delete # {0}?', $movies->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
