<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$colecionaDescription = 'Sua Coleção no lugar certo!';
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= $colecionaDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css('base.css') ?>
        <?= $this->Html->css('cake.css') ?>
        <?= $this->Html->script('jquery-1.12.0.min.js') ?>
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <nav class="top-bar expanded" data-topbar role="navigation">
            <ul class="title-area large-3 medium-4 columns">
                <li class="name">
                    <h1><a href=""><?= $this->fetch('title') ?></a></h1>
                </li>
            </ul>
            <section class="top-bar-section">
                <ul class="right">
                    <li><a  href="<?= $this->Url->build(['controller' => 'books', 'action' => 'index']) ?>"><?= __('Books') ?></a></li>
                    <li><a  href="<?= $this->Url->build(['controller' => 'movies', 'action' => 'index']) ?>"><?= __('Movies') ?></a></li>
                    <li><a  href="<?= $this->Url->build(['controller' => 'albums', 'action' => 'index']) ?>"><?= __('Albums') ?></a></li>
                    <li><a  href="<?= $this->Url->build(['controller' => 'users', 'action' => 'logout']) ?>"><?= __('Logout') ?></a></li>
                </ul>
            </section>
        </nav>
        <?= $this->Flash->render() ?>
        <section class="col-md-12 container clearfix">
            <?= $this->fetch('content') ?>
        </section>
        <footer>
        </footer>
    </body>
</html>
