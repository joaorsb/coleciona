<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Actress'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Movies'), ['controller' => 'Movies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Movie'), ['controller' => 'Movies', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="actresses index large-9 medium-8 columns content">
    <h3><?= __('Actresses') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('country_id') ?></th>
                <th><?= $this->Paginator->sort('birth') ?></th>
                <th><?= $this->Paginator->sort('death') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($actresses as $actress): ?>
            <tr>
                <td><?= $this->Number->format($actress->id) ?></td>
                <td><?= h($actress->name) ?></td>
                <td><?= $actress->has('country') ? $this->Html->link($actress->country->name, ['controller' => 'Countries', 'action' => 'view', $actress->country->id]) : '' ?></td>
                <td><?= h($actress->birth) ?></td>
                <td><?= h($actress->death) ?></td>
                <td><?= h($actress->created) ?></td>
                <td><?= h($actress->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $actress->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $actress->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $actress->id], ['confirm' => __('Are you sure you want to delete # {0}?', $actress->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
