<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Album'), ['action' => 'edit', $album->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Album'), ['action' => 'delete', $album->id], ['confirm' => __('Are you sure you want to delete # {0}?', $album->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Albums'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Album'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Labels'), ['controller' => 'Labels', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Label'), ['controller' => 'Labels', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Artists'), ['controller' => 'Artists', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artist'), ['controller' => 'Artists', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Bands'), ['controller' => 'Bands', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Band'), ['controller' => 'Bands', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Songs'), ['controller' => 'Songs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Song'), ['controller' => 'Songs', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="albums view large-9 medium-8 columns content">
    <h3><?= h($album->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($album->name) ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $album->has('user') ? $this->Html->link($album->user->name, ['controller' => 'Users', 'action' => 'view', $album->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Label') ?></th>
            <td><?= $album->has('label') ? $this->Html->link($album->label->name, ['controller' => 'Labels', 'action' => 'view', $album->label->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($album->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Release Year') ?></th>
            <td><?= h($album->release_year) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($album->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($album->modified) ?></td>
        </tr>
        <tr>
            <th><?= __('Studio') ?></th>
            <td><?= $album->studio ? __('Yes') : __('No'); ?></td>
         </tr>
        <tr>
            <th><?= __('Live') ?></th>
            <td><?= $album->live ? __('Yes') : __('No'); ?></td>
         </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Artists') ?></h4>
        <?php if (!empty($album->artists)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Birth') ?></th>
                <th><?= __('Death') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Instrument Id') ?></th>
                <th><?= __('Facebook') ?></th>
                <th><?= __('Twitter') ?></th>
                <th><?= __('Site') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($album->artists as $artists): ?>
            <tr>
                <td><?= h($artists->id) ?></td>
                <td><?= h($artists->name) ?></td>
                <td><?= h($artists->country_id) ?></td>
                <td><?= h($artists->birth) ?></td>
                <td><?= h($artists->death) ?></td>
                <td><?= h($artists->description) ?></td>
                <td><?= h($artists->instrument_id) ?></td>
                <td><?= h($artists->facebook) ?></td>
                <td><?= h($artists->twitter) ?></td>
                <td><?= h($artists->site) ?></td>
                <td><?= h($artists->created) ?></td>
                <td><?= h($artists->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Artists', 'action' => 'view', $artists->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Artists', 'action' => 'edit', $artists->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Artists', 'action' => 'delete', $artists->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artists->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Bands') ?></h4>
        <?php if (!empty($album->bands)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Country Id') ?></th>
                <th><?= __('Started At') ?></th>
                <th><?= __('Ended At') ?></th>
                <th><?= __('Genre Id') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Site') ?></th>
                <th><?= __('Facebook') ?></th>
                <th><?= __('Twitter') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($album->bands as $bands): ?>
            <tr>
                <td><?= h($bands->id) ?></td>
                <td><?= h($bands->name) ?></td>
                <td><?= h($bands->country_id) ?></td>
                <td><?= h($bands->started_at) ?></td>
                <td><?= h($bands->ended_at) ?></td>
                <td><?= h($bands->genre_id) ?></td>
                <td><?= h($bands->description) ?></td>
                <td><?= h($bands->site) ?></td>
                <td><?= h($bands->facebook) ?></td>
                <td><?= h($bands->twitter) ?></td>
                <td><?= h($bands->created) ?></td>
                <td><?= h($bands->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Bands', 'action' => 'view', $bands->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Bands', 'action' => 'edit', $bands->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Bands', 'action' => 'delete', $bands->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bands->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Songs') ?></h4>
        <?php if (!empty($album->songs)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Duration') ?></th>
                <th><?= __('Studio') ?></th>
                <th><?= __('Live') ?></th>
                <th><?= __('Lyrics') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($album->songs as $songs): ?>
            <tr>
                <td><?= h($songs->id) ?></td>
                <td><?= h($songs->name) ?></td>
                <td><?= h($songs->duration) ?></td>
                <td><?= h($songs->studio) ?></td>
                <td><?= h($songs->live) ?></td>
                <td><?= h($songs->lyrics) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Songs', 'action' => 'view', $songs->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Songs', 'action' => 'edit', $songs->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Songs', 'action' => 'delete', $songs->id], ['confirm' => __('Are you sure you want to delete # {0}?', $songs->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
