<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Movie Entity.
 *
 * @property int $id
 * @property string $title
 * @property string $original_title
 * @property \Cake\I18n\Time $release_year
 * @property string $sinopsis
 * @property int $studio_id
 * @property \App\Model\Entity\Studio $studio
 * @property int $movie_region
 * @property \Cake\I18n\Time $duration
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Actor[] $actors
 * @property \App\Model\Entity\Actress[] $actresses
 * @property \App\Model\Entity\Director[] $directors
 */
class Movie extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
