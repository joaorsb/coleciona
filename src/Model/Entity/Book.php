<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Book Entity.
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $publisher_id
 * @property \App\Model\Entity\Publisher $publisher
 * @property int $number_of_pages
 * @property string $sinopsis
 * @property string $isbn
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Author[] $authors
 * @property \App\Model\Entity\Genre[] $genres
 * @property \App\Model\Entity\Tag[] $tags
 */
class Book extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
