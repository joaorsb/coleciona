<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Band Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $country_id
 * @property \App\Model\Entity\Country $country
 * @property \Cake\I18n\Time $started_at
 * @property \Cake\I18n\Time $ended_at
 * @property int $genre_id
 * @property \App\Model\Entity\Genre $genre
 * @property string $description
 * @property string $site
 * @property string $facebook
 * @property string $twitter
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Album[] $albums
 * @property \App\Model\Entity\Artist[] $artists
 * @property \App\Model\Entity\Song[] $songs
 */
class Band extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
