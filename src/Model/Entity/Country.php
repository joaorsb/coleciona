<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Country Entity.
 *
 * @property int $id
 * @property string $name
 * @property int $continent_id
 * @property \App\Model\Entity\Continent $continent
 * @property string $description
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Actor[] $actors
 * @property \App\Model\Entity\Actress[] $actresses
 * @property \App\Model\Entity\Artist[] $artists
 * @property \App\Model\Entity\Author[] $authors
 * @property \App\Model\Entity\Band[] $bands
 * @property \App\Model\Entity\Director[] $directors
 * @property \App\Model\Entity\Label[] $labels
 * @property \App\Model\Entity\Publisher[] $publishers
 * @property \App\Model\Entity\Studio[] $studios
 * @property \App\Model\Entity\Translator[] $translators
 */
class Country extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
