<?php
namespace App\Model\Table;

use App\Model\Entity\Country;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Countries Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Continents
 * @property \Cake\ORM\Association\HasMany $Actors
 * @property \Cake\ORM\Association\HasMany $Actresses
 * @property \Cake\ORM\Association\HasMany $Artists
 * @property \Cake\ORM\Association\HasMany $Authors
 * @property \Cake\ORM\Association\HasMany $Bands
 * @property \Cake\ORM\Association\HasMany $Directors
 * @property \Cake\ORM\Association\HasMany $Labels
 * @property \Cake\ORM\Association\HasMany $Publishers
 * @property \Cake\ORM\Association\HasMany $Studios
 * @property \Cake\ORM\Association\HasMany $Translators
 */
class CountriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('countries');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Continents', [
            'foreignKey' => 'continent_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Actors', [
            'foreignKey' => 'country_id'
        ]);
        $this->hasMany('Actresses', [
            'foreignKey' => 'country_id'
        ]);
        $this->hasMany('Artists', [
            'foreignKey' => 'country_id'
        ]);
        $this->hasMany('Authors', [
            'foreignKey' => 'country_id'
        ]);
        $this->hasMany('Bands', [
            'foreignKey' => 'country_id'
        ]);
        $this->hasMany('Directors', [
            'foreignKey' => 'country_id'
        ]);
        $this->hasMany('Labels', [
            'foreignKey' => 'country_id'
        ]);
        $this->hasMany('Publishers', [
            'foreignKey' => 'country_id'
        ]);
        $this->hasMany('Studios', [
            'foreignKey' => 'country_id'
        ]);
        $this->hasMany('Translators', [
            'foreignKey' => 'country_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['continent_id'], 'Continents'));
        return $rules;
    }
}
