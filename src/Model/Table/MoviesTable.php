<?php
namespace App\Model\Table;

use App\Model\Entity\Movie;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Movies Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Studios
 * @property \Cake\ORM\Association\BelongsToMany $Actors
 * @property \Cake\ORM\Association\BelongsToMany $Actresses
 * @property \Cake\ORM\Association\BelongsToMany $Directors
 */
class MoviesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('movies');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Studios', [
            'foreignKey' => 'studio_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Actors', [
            'foreignKey' => 'movie_id',
            'targetForeignKey' => 'actor_id',
            'joinTable' => 'movies_actors'
        ]);
        $this->belongsToMany('Actresses', [
            'foreignKey' => 'movie_id',
            'targetForeignKey' => 'actress_id',
            'joinTable' => 'movies_actresses'
        ]);
        $this->belongsToMany('Directors', [
            'foreignKey' => 'movie_id',
            'targetForeignKey' => 'director_id',
            'joinTable' => 'movies_directors'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['studio_id'], 'Studios'));
        return $rules;
    }
}
