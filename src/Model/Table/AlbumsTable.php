<?php
namespace App\Model\Table;

use App\Model\Entity\Album;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Albums Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Labels
 * @property \Cake\ORM\Association\BelongsToMany $Artists
 * @property \Cake\ORM\Association\BelongsToMany $Bands
 * @property \Cake\ORM\Association\BelongsToMany $Songs
 */
class AlbumsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('albums');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Labels', [
            'foreignKey' => 'label_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Artists', [
            'foreignKey' => 'album_id',
            'targetForeignKey' => 'artist_id',
            'joinTable' => 'albums_artists'
        ]);
        $this->belongsToMany('Bands', [
            'foreignKey' => 'album_id',
            'targetForeignKey' => 'band_id',
            'joinTable' => 'albums_bands'
        ]);
        $this->belongsToMany('Songs', [
            'foreignKey' => 'album_id',
            'targetForeignKey' => 'song_id',
            'joinTable' => 'albums_songs'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');
        
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['label_id'], 'Labels'));
        return $rules;
    }
}
